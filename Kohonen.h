    // typ prostego wektora danych
    // la przyk�adu je�li mamy krotki danych jak:
    // {(1,2,3),(2,3,4),(3,4,5),(4,5,6),(5,6,7), ... }
    // te dane mog� opisywa� pozycje pixel i to jest kolor
    // V_d opisuje typ przykad (1,2,3  
    typedef vector < double > V_d;

    // configuracja Gausa ma funkcje
    typedef neural_net::Gauss_function
      < double, double, int > G_a_f;
    // Aktywacja funkcji dla neuron�w z parametrami
    G_a_f g_a_f ( 2.0, 1 );

    //  przygotuj Euklidesowy dystans funkcji
    typedef distance::Euclidean_distance_function
      < V_d > E_d_t;
    E_d_t e_d;

    // tutaj aktywacj Gausa i euklidesa dystans jest wybierany do budowy kohonnenowego neurona
    typedef neural_net::Basic_neuron
      < G_a_f, E_d_t > Kohonen_neuron;

    //  Neuron kohonnena jest uzywany dla konstrukcji neurona kohonena sieci
    typedef neural_net::Rectangular_container
      < Kohonen_neuron > Kohonen_network;
    Kohonen_network kohonen_network;

    // generuje siec inicjalizowan� przez dane 
    neural_net::generate_kohonen_network
    //<
    //  wektor < V_d >,
    //  siec Kohonena,
    //  neural_net::Internal_randomize
    //>
    ( R, C, g_a_f, e_d, data, kohonen_network, IR );
	
	// Przy pomocy https://accu.org/index.php/journals/1378