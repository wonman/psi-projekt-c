namespace MotionDetectorSample
{
    partial class MotionRegionsForm
    {
        private System.ComponentModel.IContainer components = null;
//Nadpisywanie Dispose w celu usuwania
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }
        #region Windows Form Designer generated code
//inicjalizacja komponentów

        }

        #endregion

        private DefineRegionsControl defineRegionsControl;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ToolStripButton rectangleButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton clearButton;
    }
}