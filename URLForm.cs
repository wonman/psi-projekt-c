//plik URL FORM na ma celu pobieranie �r�d�a ze stron wwww do wykorzytywania w programie.
//Za�o�eniem jest danie mo�liwo�ci pobrania pliku .jpg oraz pliku video.
//Pozwoli nam to na badaniu zjawiska wykrywania ruchu na �w plikach.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MotionDetectorSample
{
    public partial class URLForm : Form
    {
        private string url;

        //  Wybieranie URL
        public string URL
        {
            get { return url; }
        }

        // Pokazywanie 'URRL" w komboboxie
        public string[] URLs
        {
            set
            {
                urlBox.Items.AddRange( value );
            }
        }

        // Opis
        public string Description
        {
            get { return descriptionLabel.Text; }
            set { descriptionLabel.Text = value; }
        }

         
        public URLForm( )
        {
            InitializeComponent( );
        }

        // przycisk OK do potwierdzenia
        private void okButton_Click( object sender, EventArgs e )
        {
            url = urlBox.Text;

        }
    }
}