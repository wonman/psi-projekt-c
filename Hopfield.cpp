//Sieć Hopfielda, główne funkcje tej metody
//  @@ napiz wagę macierzy
    cout<<"The weight matrix:"<<endl<<endl;
    for(j=0;j<n;j++)
    {
        for(i=0;i<n;i++)
            printf("%2d ",w[j*n+i]);
        cout<<endl;
    }
    cout<<endl;

    cout<<"Pattern-recognition Test:"<<endl<<endl;
    //  @@ Wybieranie jednego z wzorców do trenowania losowo
    int selectedPattern=rand()%m;
    cout<<"Test pattern selected:"<<endl;
    for(i=0;i<n;i++)
    {
        cout<<pattern[selectedPattern*n+i];
    }
    cout<<endl<<endl;
    
    int errorPercentage=10;
    cout<<"Initial network state:"<<endl;
    cout<<"The test pattern w/ "<<errorPercentage<<"% error added:"<<endl;
    int* neuron=new int[n];      // @@ konkretny stan sieci
    int* neuron_prev=new int[n]; // @@ popzedni stan sieci
    for(i=0;i<n;i++)
    {
        neuron[i]=pattern[selectedPattern*n+i];
        if(rand()%100<errorPercentage) neuron[i]=1-neuron[i];
        cout<<neuron[i];
        neuron_prev[i]=neuron[i]; // initially prev state=current @@ poczatkowo wczesniejszy state = current
    }
    cout<<endl<<endl;

    //  @@ jeeli stan sieci zostanie nie zmieniony dla kroków to oznacza że siec jest skupiona na odpowiedzi wiec potem wychodzi do pętli i drukuje  ostatni stan 
    
    int ctr_unchg=0;

    // @@ licznk pętli  by upewnić stop porprostu w razie jeśli sieć stanie się chotycna lub cyklicna
    
    int ctr=0;
    
    while(ctr_unchg<100 && ctr<1000) // max 1000 loops allowed
    {

        //  Pierwszy wybór dla ulepszenia sieci
        for(k=0;k<n;k++) //  ulepszene calej sieci
        {
            //seryjny losowe ulepszanie
            // losowo wybierany neuron i ulepszany sobie wartosc
            j=rand()%n;
            sum=0;
            for(i=0;i<n;i++)
                if(i!=j)
                    sum+=neuron[i]*w[j*n+i];
            if(sum>=0)
                neuron[j]=1;
            else
                neuron[j]=0;
        }