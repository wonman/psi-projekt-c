// Klasa MotionDetectorSample s�u�y do wykrywania ruchu, jest to rozbudowana klasa. 
// Og�lne podej�cie to stosowanie tr�jk�t�w. Okre�la si� ich kolory, wielkosc. 
// Tr�jk�t nale�y do prymityw�w w grafice, z uwagi na prostote oraz posiadanie powierzchni 
// s� wykorzystywane w tracingu. Po zdefiniowaniu tr�jk�t�w, w daljszej cz�ci okre�la sie 
// zalezno�c ich w stosunku do klienta

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MotionDetectorSample
{
    // Rysowanie modelu
    public enum DrawingMode
    {
        None,
        Rectangular
    }

    // Uzycie delegatu do powiadomienia o nowym tr�jk�cie
    public delegate void NewRectangleHandler( object sender, Rectangle rect );

    // Kontrola do zdefiniowania ruchu regionu
    public partial class DefineRegionsControl : System.Windows.Forms.Control
    {
        // kolor kraw�dzi
        private Color borderColor = Color.Black;
        // kolor t�a
        private Color backColor = Color.FromArgb( 96, 96, 96 );
        // kolor zaznaczonego tr�jk�ta
        private Color selectionColor = Color.Yellow;
        // kolor kolor tr�jk�ta
        private Color rectsColor = Color.FromArgb( 0, 255, 0 );
        // obrazek t�a
        private Bitmap backImage = null;

        // zbi�r tr�jk�t�w
        private List<Rectangle> rectangles = new List<Rectangle>( );

        private DrawingMode drawingMode = DrawingMode.None;
        private bool dragging = false;

        private Point startPoint;
        private Point endPoint;

        //Obrazek t�a
        public new Bitmap BackgroundImage
        {
            get { return backImage; }
            set { backImage = value; }
        }

        // Tryb rysowania
        public DrawingMode DrawingMode
        {
            get { return drawingMode; }
            set
            {
                drawingMode = value;

                this.Cursor = ( drawingMode == DrawingMode.None ) ? Cursors.Default : Cursors.Cross;
            }
        }

        // Tablica tr�jk�t�w
        public Rectangle[] Rectangles
        {
            get
            {
                Rectangle[] rects = new Rectangle[rectangles.Count];
                rectangles.CopyTo( rects );
                return rects;
            }
            set
            {
                rectangles.Clear( );

                if ( value != null )
                {
                    rectangles.AddRange( value );
                }
                Invalidate( );
            }
        }

        // Zdarzenie do powiadomywania o nowych tr�jk�tach
        public event NewRectangleHandler OnNewRectangle;
        
         
        public DefineRegionsControl( )
        {
            InitializeComponent( );

            //  aktualizacja styl�w kontrolo
            SetStyle( ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer |
                ControlStyles.ResizeRedraw | ControlStyles.UserPaint, true );
        }

        //  Usuwanie region�w
        public void RemoveAllRegions( )
        {
            rectangles.RemoveRange( 0, rectangles.Count );
            this.Invalidate( );
        }

		// Malowanie kotrolki
        protected override void OnPaint( PaintEventArgs pe )
        {
            Graphics  g = pe.Graphics;
            Rectangle rect = this.ClientRectangle;

            // rysowanie tr�jk�ta 
            using ( Pen pen = new Pen( borderColor, 1 ) )
            {
                g.DrawRectangle( pen, rect.X, rect.Y, rect.Width - 1, rect.Height - 1 );
            }

            // rysowanie t�a
            if ( backImage != null )
            {
                g.DrawImage( backImage, 1, 1, rect.Width - 2, rect.Height - 2 );
            }
            else
            {
                using ( Brush backBrush = new SolidBrush( backColor ) )
                {
                    g.FillRectangle( backBrush, 1, 1, rect.Width - 2, rect.Height - 2 );
                }
            }

            //rysowanie tr�jk�ta 
            using ( Pen pen = new Pen( rectsColor, 1 ) )
            {
                foreach ( Rectangle r in rectangles )
                {
                    g.DrawRectangle( pen, r.X + 1, r.Y + 1, r.Width - 1, r.Height - 1 );
                }
            }

            base.OnPaint( pe );
        }

        //  Myszka w d�
        protected override void OnMouseDown( MouseEventArgs e )
        {
            //   sprawdzanie trybu rysowania
            if ( drawingMode == DrawingMode.Rectangular )
            {
                //  sprawdzanie typu prycisku
                if ( e.Button == MouseButtons.Left )
                {
                    //   prze�anczanie do przeci�gania 
                    dragging = true;
                    this.Capture = true;

                    // ustaw inicjalizowany start i zakonczenie punkt�w
                    startPoint.X = endPoint.X = e.X;
                    startPoint.Y = endPoint.Y = e.Y;
                    //  rysuj dany tr�jk�t
                    ControlPaint.DrawReversibleFrame( new Rectangle( e.X, e.Y, 1, 1 ), Color.Green, FrameStyle.Dashed );
                }
                else if ( e.Button == MouseButtons.Right )
                {
                }
            }
        }

        // Myszka w g�re
        protected override void OnMouseUp( MouseEventArgs e )
        {
            //  sprawdz rysowanie i przeci�ganie trybu
            if ( ( drawingMode == DrawingMode.Rectangular ) && ( dragging == true ) )
            {
                //   zatrzymanie przeci�gania
                dragging = false;
                this.Capture = false;

                //  reset rysowania
                drawingMode = DrawingMode.None;
                this.Cursor = Cursors.Default;

                //  usun zaznaczony trojkat
                DrawSelectionRectangle( );

                //  dostan znoralizowane pkty
                NormalizePoints( ref startPoint, ref endPoint );

                //  sprawdz pukty s� wewn�trz kotrolera
                CheckPointsInClient( ref startPoint );
                CheckPointsInClient( ref endPoint );

                // dodaj trojk�t do kolekcji 
                Rectangle rect = new Rectangle( startPoint.X - 1, startPoint.Y - 1, endPoint.X - startPoint.X + 1, endPoint.Y - startPoint.Y + 1 );
                rectangles.Add( rect );

                // powiadam klienta o nowym trojk�cie 
                if ( OnNewRectangle != null )
                {
                    OnNewRectangle( this, rect );
                }

                // ponownie na rysuj kontrolki
                this.Invalidate( );
            }
        }

        // Ruch myszki
        protected override void OnMouseMove( MouseEventArgs e )
        {
            if ( dragging == true )
            {
                // usun stary tr�jk�t
                DrawSelectionRectangle( );

                endPoint.X = e.X;
                endPoint.Y = e.Y;

                // narysuj nowy trojkat
                DrawSelectionRectangle( );
            }
        }

        // narysuj odwr�coon zaznacozny trojkat 
        private void DrawSelectionRectangle( )
        {
            Point start = startPoint;
            Point end   = endPoint;

            // do normalizacji
            NormalizePoints( ref start, ref end );

            // sprawdz czy pkty wewn�trz kontrolki
            CheckPointsInClient( ref start );
            CheckPointsInClient( ref end );

            // konwersja klienta kordynat�w na ekranowe
            Point screenStartPoint = this.PointToScreen( start);
            Point screenEndPoint   = this.PointToScreen( end );

            // rysuj tr�jk�ta
            ControlPaint.DrawReversibleFrame(
                new Rectangle(
                    screenStartPoint.X, screenStartPoint.Y,
                    screenEndPoint.X - screenStartPoint.X + 1, screenEndPoint.Y - screenStartPoint.Y + 1 ),
                selectionColor, FrameStyle.Dashed );
        }

        //  normalizuj pkty, 1' pkt bedzie trzymac male kordynaty
        private void NormalizePoints( ref Point point1, ref Point point2 )
        {
            Point t1 = point1;
            Point t2 = point2;

            point1.X = Math.Min( t1.X, t2.X );
            point1.Y = Math.Min( t1.Y, t2.Y );
            point2.X = Math.Max( t1.X, t2.X );
            point2.Y = Math.Max( t1.Y, t2.Y );
        }

        // zapewnij pkt jest w strefie klienta
        private void CheckPointsInClient( ref Point point )
        {
            if ( point.X < 1 )
            {
                point.X = 1;
            }
            if ( point.Y < 1 )
            {
                point.Y = 1;
            }
            if ( point.X >= ClientRectangle.Width - 1 )
            {
                point.X = ClientRectangle.Width - 2;
            }
            if ( point.Y >= ClientRectangle.Height - 1 )
            {
                point.Y = ClientRectangle.Height - 2;
            }
        }
    }
}
